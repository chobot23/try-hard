## holistic, systems based approach to learning

fundmental ideas:
    -learning is the act of altering one's perspective of a domain across time from less predictable to more predictable
    -control is a holistic understanding of a domain across time
    -the largest flaw of modern education lies in the false equivalence of categories and labels
    -systematic labeling creates categories; systematic categorization creates labels
    -general ai is any perfectly optimized system for learning across time
    -the utility of generalized ai will outweigh the danger unless provided an alternative
    -the optimization of generalized ai is something that cannot be stopped once started

solution:
    create a system that can analogize any situation's set of choices to that of the set of choices of the most generic situation that can still retain holistic meaning, and back.
    
    thing 
        smallest unit of change
    choice
        the thing
        not the thing
    move
        start position
        end position
        knowlege tree limb
    path
        start point
        end point
        list of list of angles for calculating distance   // for this imagine that you have a 1-handed clock going around pi/2 while moving towards the next point in the information sphere some algorithm, each move 
         
    meta-choice
        (changes the configuration of the state of one variable in one meaningful way defined by the state (most important))
        effected variable
        new value
        old value
        possible meta-paths sorted by shortest distance
    action 
        meta-choices
    object
        changeAllowed
        properties
        actions
    actor
        short term motivations
        long term motivations
        meta / transcendent motivations (will / amalgamatation of short and long term motivations) 
        possible action sorted on the most positive effect (on actor) ny enacting the action for the least negative effect
    environment
        objects
        actors
    state
        environment
        actors
    situation
        currrent state
        list of potential previous state sorted on number of actions from ideal state
        list of potential next state sorted on number of actions from least ideal state
    meta-situation
        list of situations sorted by goal parameters
        