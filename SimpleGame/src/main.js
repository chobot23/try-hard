// John Chohon, Matt Bennett, Nick Johs


function testFunc() {
    // loadWasm('simple_game')
    //     .then(instance => {
    //         const exports = instance.exports;
    //         //console.log(exports._triple(2));
    //     });

}

class GameObject {
    // a list of functions 
    constructor(modelUrl, endoFunctions, exoFunctions) {
        this.modelUrl = modelUrl;
        this.endoFunctions = endoFunctions;
        this.exoFunctions = exoFunctions;
        getRenderForView = (view) => {
            
        }
    }
}

class Location {
    constructor(originPoint, points) {
        this.originPoint = originpoint;
        this.boundingPoints = points;
    }
}

class Map{
    constructor() {
        this.getGameObjectsforLocation = (location) => {
            
        }
    }
}

class View {
    constructor(x,y,z,direction, viewAngle,distance) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.direction = direction;
        this.distance = distance;
    }
}

class Player {
    constructor() {
        this.view = new View();
        this.move = (direction, distance) => {
            // implement logic here
        }
    }
}

class GameInstance {
    constructor() {
        this.isPaused = false;
        this.currentInput = {
            up: false,
            down: false,
            left: false,
            right: false,
            space: false,
            refresh: () => {
                this.currentInput.up = false;
                this.currentInput.down = false;
                this.currentInput.right = false;
                this.currentInput.left = false;
                this.currentInput.space = false;
            }
        };
        this.init = () => {
            const canvas = document.getElementById('user-view');
            const width = canvas.offsetWidth;
            const height = canvas.offsetHeight;
            // set up controls
            document.addEventListener('keydown', (event) => {
                switch (event.key) {
                    case 'ArrowUp':
                        this.currentInput.up = true;
                        break;
                    case 'ArrowDown':
                        this.currentInput.down = true;
                        break;
                    case 'ArrowRight':
                        this.currentInput.right = true;
                        break;
                    case 'ArrowLeft':
                        this.currentInput.left = true;
                        break;
                    case ' ':
                        this.currentInput.space = true;
                        break;
                }
            });
            document.addEventListener('keyup', (event) => {
                switch (event.key) {
                    case 'ArrowUp':
                        this.currentInput.up = false;
                        break;
                    case 'ArrowDown':
                        this.currentInput.down = false;
                        break;
                    case 'ArrowRight':
                        this.currentInput.right = false;
                        break;
                    case 'ArrowLeft':
                        this.currentInput.left = false;
                        break;
                    case ' ':
                        this.currentInput.space = false;
                        break;
                }
            });
        };
    }
}


// entry point
function jsMain() {
    const GAME_INSTANCE = new GameInstance();
    GAME_INSTANCE.init();
}


//WASM 
function loadWasm(fn) {
    const fileName = `./src/${fn}.wasm`;
    return fetch(fileName)
        .then(response => response.arrayBuffer())
        .then(buffer => WebAssembly.compile(buffer))
        .then(module => {
            imports = {}; // may need to add in future
            imports.env = {};
            imports.env.memoryBase = 0;
            imports.env.tableBase = 0;
            imports.env.memory = new WebAssembly.Memory({
                initial: 256
            });
            imports.env.table = new WebAssembly.Table({
                initial: 0,
                element: 'anyfunc'
            });
            return new WebAssembly.Instance(module, imports);
        });
}